from utils import *
from snippets.loader import *
from pprint import PrettyPrinter
from pathlib import Path
from load_data import VOCDataset, get_items, label_map, device
# Good formatting when printing the APs for each class and mAP
pp = PrettyPrinter()

_2007_root = Path("/home/yyr/data/VOCdevkit/VOC2007")
val_items   = get_items(_2007_root, 'val')
logger.info(f'\n{len(val_items)} validation images')
val_dataset = VOCDataset(val_items)

keep_difficult = True
batch_size = 16
workers = 4

model = torch.load('ssd300.pth', map_location=device)

val_loader = torch.utils.data.DataLoader(val_dataset, batch_size=batch_size, shuffle=False,
                                          collate_fn=val_dataset.collate_fn, num_workers=workers, pin_memory=True)

def evaluate(val_loader, model):
    model.eval()

    # Lists to store detected and true boxes, labels, scores
    det_boxes = list()
    det_labels = list()
    det_scores = list()
    true_boxes = list()
    true_labels = list()
    true_difficulties = list()  # it is necessary to know which objects are 'difficult', see 'calculate_mAP' in utils.py

    with torch.no_grad():
        # Batches
        for i, (images, boxes, labels, difficulties) in enumerate(Tqdm(val_loader, desc='Evaluating')):
            images = images.to(device)  # (N, 3, 300, 300)
            predicted_locs, predicted_scores = model(images)
            det_boxes_batch, det_labels_batch, det_scores_batch = model.detect_objects(predicted_locs, predicted_scores,
                                                                                       min_score=0.5, max_overlap=0.45,
                                                                                       top_k=200)
            # Store this batch's results for mAP calculation
            boxes = [b.to(device) for b in boxes]
            labels = [l.to(device) for l in labels]
            difficulties = [d.to(device) for d in difficulties]

            det_boxes.extend(det_boxes_batch)
            det_labels.extend(det_labels_batch)
            det_scores.extend(det_scores_batch)
            true_boxes.extend(boxes)
            true_labels.extend(labels)
            true_difficulties.extend(difficulties)

        # Calculate mAP
        APs, mAP = calculate_mAP(det_boxes, det_labels, det_scores, true_boxes, true_labels, true_difficulties)

    # Print AP for each class
    pp.pprint(APs)
    print('\nMean Average Precision (mAP): %.3f' % mAP)

if __name__ == '__main__':
    evaluate(val_loader, model)
