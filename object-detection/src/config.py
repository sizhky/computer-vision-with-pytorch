FOUND_FIELDS = ['# Eligible Employees',
 '# Ineligible Employees',
 'Address(es) (or list on additional sheet of paper)',
 'Billing Address (If Different)',
 'City',
 'Classes Excluded',
 'Contact Name',
 'Did you have any employees other than yourself and your spouse during the preceding calendar year?',
 'Domestic Partner Coverage',
 'E-Mail',
 'Effective Date',
 'Fax Number',
 'Group Name to appear on ID card (maximum 30 characters)',
 "Group's Full Legal Name",
 "Have Workers' Comp",
 'Hours per Week',
 'If the majority of your employees are not located in your state of application',
 'Industry Code',
 'Internet access?',
 'Is the group subject to ERISA',
 'List all subsidiaries to be included:',
 'Medical Benefit Plan Option',
 'Multi-Location Group*',
 "Name of Workers' Compensation Carrier",
 'Names of Owners/Partners (if applicable)',
 "Names of Owners/Partners not covered by Workers' Comp",
 "Names of Owners/Partners not covered by Workers' Compensation",
 'Names of Persons currently on COBRA/Continuation, and/or Short/Long Term Disability',
 'Nature of Business/Organization',
 'Number of Employees Termed in last 12 Months',
 'Number of Locations',
 'Number of Persons currently on COBRA/Continuation, and/or Short/Long Term Disability (employees/dependents)',
 'Number of Years in Existence',
 'Organization Type',
 'Phone Number',
 'State',
 'Street Address',
 'Tax ID Number',
 'Total # Employees',
 'Waiting Period for new hires (Waiting period for medical coverage cannot exceed 90 days)',
 'Waiting Period waived for initial enrollees',
 'Will there be an Eligibility Waiting Period for New Hires',
 'X Calendar Year',
 'Zip Code']
# FOUND_FIELDS = FOUND_FIELDS[:5]
 
IMAGE_SIZE = 500, 1700

R, C = 25,17
H, W = IMAGE_SIZE
sub_h = 26
sub_w = 210
sub_crop_scale = 1.05
stride_h, stride_w = int((H-sub_h)/(R-1)), int((W-sub_w)/(C-1))
HEATMAP_INFO = (R,C,W,H,sub_crop_scale,sub_h,sub_w,stride_h,stride_w)

# R, C = 25,10
# H, W = IMAGE_SIZE

# sub_h = 26
# sub_w = 400
# sub_crop_scale = 1.05
# stride_h, stride_w = int((H-sub_h)/(R-1)), int((W-sub_w)/(C-1))
# HEATMAP_INFO_2 = (R,C,W,H,sub_crop_scale,sub_h,sub_w,stride_h,stride_w)

device = 'cuda'