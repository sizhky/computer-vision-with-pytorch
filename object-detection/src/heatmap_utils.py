from ocr.loader import *
from ocr.cv2utils import *
import imgaug.augmenters as iaa
import imgaug as ia
from imgaug.augmentables.bbs import BoundingBox
from collections import defaultdict
import torch
from torch.nn import functional  as F
from .config import *
print('HEAT',FOUND_FIELDS)

def from_imgaug_bbs(bbs): return [BB(bb.x1,bb.y1,bb.x2,bb.y2) for bb in bbs]
sometimes = lambda aug, p=0.5: iaa.Sometimes(p, aug)

P = 0.5
aug = iaa.Sequential([
    sometimes(iaa.OneOf([
        iaa.GaussianBlur(sigma=(0, 1.0)),
        iaa.MotionBlur()]),
              p=P),
    iaa.Pad(
            percent=((0.0,0.015), (0.0,0.04), (0.0,0.015), (0.0,0.04)),
            pad_mode='constant',
            pad_cval=(255)
    ),
    sometimes(iaa.Affine(
            scale={"x": (0.8, 1.2), "y": (0.8, 1.2)}, # scale images to 80-120% of their size, individually per axis
            rotate=(-2, 2), # rotate by -x to +x degrees
            shear=(-2, 2), # shear by -x to +x degrees
            order=[0, 1], # use nearest neighbour or bilinear interpolation (fast)
            cval=(255), # if mode is constant, use a cval between 0 and 255
            mode='constant' # use any of scikit-image's warping modes (see 2nd image from the top for examples)
        ), p=P),
    # sometimes(iaa.ElasticTransformation(alpha=(20, 30), sigma=2), p=P),
    iaa.Resize({"height": 500, "width": 1700})
    # sometimes(iaa.PerspectiveTransform(scale=(0.01, 0.1), cval=255), p=P),
])
aug2 = iaa.Sequential(iaa.Resize({"height": 500, "width": 1700}))

####################################################################################

def IOU(b1, b2):
    x1,y1,x2,y2 = b1
    x3,y3,x4,y4 = b2
    assert x1<=x2 and x3<=x4 and y1<=y2 and y3<=y4
    A_b1 = (x2-x1+1) * (y2-y1+1)
    A_b2 = (x4-x3+1) * (y4-y3+1)
    xa, ya = max(x1,x3), max(y1,y3)
    xb, yb = min(x2,x4), min(y2,y4)
    intersection = max(0, xb-xa+1) * max(0, yb-ya+1)
    iou = intersection / (A_b1 + A_b2 - intersection)
    return iou

def bb2hm(bb):
    R,C,W,H,sub_crop_scale,sub_h,sub_w,stride_h,stride_w = heatmap_info
    x,y,X,Y = bb
    iou_map = np.zeros((R,C))
    for i in range(R):
        for j in range(C):
            b1 = BB(x,y,X,Y)
            b2 = (j*stride_w, i*stride_h, j*stride_w+sub_w, i*stride_h+sub_h)
            val = IOU(b1, b2) if b1.w > 5 else 0 #  print(b1, b2, val)
            iou_map[i,j] = val
    iou_map = iou_map == iou_map.max() if iou_map.max() > 0 else iou_map
    return iou_map

def hm2bb(hm):
    argmax = np.argmax(hm)
    i,j = np.unravel_index(argmax, shape=(R,C))
    bb = (j*stride_w, i*stride_h, j*stride_w+sub_w, i*stride_h+sub_h)
    return BB(bb)

def ancs():
    R,C,W,H,sub_crop_scale,sub_h,sub_w,stride_h,stride_w = heatmap_info
    anc_bbs = []
    for i in range(R):
        for j in range(C):
            anc_bb = BB(j*stride_w, i*stride_h, j*stride_w+sub_w, i*stride_h+sub_h)
            anc_bbs.append(anc_bb)
    return anc_bbs

def getHeatmaps(bbs):
    heatmap_stack = []
    for bb in bbs:
        heatmap_stack.append(bb2hm(bb))
    heatmap_stack = np.array(heatmap_stack)
    return heatmap_stack

def compute_background_heatmap(heatmap_stack):
    assert len(heatmap_stack.shape) == 3, 'input should be CxHxW'
    background = np.clip(1 - heatmap_stack.sum(0), 0, 1)[None]
    return np.r_[background, heatmap_stack]

def img2label(im, bbs):
    R,C,W,H,sub_crop_scale,sub_h,sub_w,stride_h,stride_w = heatmap_info
    classification_map = np.zeros((len(FOUND_FIELDS),R,C))
    regression_map = np.zeros(((1+len(FOUND_FIELDS))*4,R,C))
    found_fld_bbs, found_anchors, found_fld_deltas = defaultdict(list), defaultdict(list), defaultdict(list)
    for obj_ix, fld in enumerate(FOUND_FIELDS):
        iou_map = np.zeros((R,C))
        bb_fld = bbs[obj_ix]
        for i in range(R):
            for j in range(C):
                anc_bb = BB(j*stride_w, i*stride_h, j*stride_w+sub_w, i*stride_h+sub_h)
                iou_val = IOU(anc_bb, bb_fld)
                if iou_val > 0.1:
                    found_fld_bbs[fld].append(bb_fld) if fld not in found_fld_bbs else 0
                    found_anchors[fld].append(anc_bb)
                    # found_flds[fld].append(fld)
                    dx, dy = anc_bb.x - bb_fld.x, anc_bb.y - bb_fld.y
                    dX, dY = anc_bb.X - bb_fld.X, anc_bb.Y - bb_fld.Y
                    found_fld_deltas[fld].append(BB(dx,dy,dX,dY))
                    regression_map[(obj_ix+1)*4:(obj_ix+2)*4,i,j] = (dx/W,dy/H,dX/W,dY/H)
                    # show(im, bbs=[bb_fld, anc_bb], bb_colors=[(0,0,255),(255,0,0)], title=str((dx,dy,dX,dY)))
                    # regression_map[obj_ix,:,i,j] = (dx,dy,dX,dY) if iou_val > 0.1 else 0
                    iou_map[i,j] = iou_val
        iou_map = iou_map == iou_map.max() if iou_map.max() > 0 else iou_map
        # if iou_map.max() > 0.1: show(iou_map, title=fld, sz=5)
        classification_map[obj_ix] = iou_map
        # regression_map[1+obj_ix] = regression_map[1+obj_ix]*iou_map
    # show(im, bbs=bbs+found_fld_bbs+found_anchors, bb_colors=[(0,255,0)]*len(bbs)+[(0,0,255)]*len(found_fld_bbs)+[(255,0,0)]*len(found_anchors))
    # show(im, bbs=flatten(list(found_fld_bbs.values())), texts=list(found_fld_bbs.keys()))
    classification_map = compute_background_heatmap(classification_map)
    return classification_map, regression_map.reshape(-1, R, C), found_fld_bbs, found_fld_deltas, found_anchors

def random_crop_and_bbs(IMS, BBS, ix=None):
    ix = randint(len(IMS)) if ix is None else ix
    im = IMS[ix]
    h, w = im.shape
    x, y = 0, randint(h-500)
    crop = crop_from_bb(im, (x,y,w,y+500))
    return crop, [BB(bb.x-x,bb.y-y,bb.w+bb.x-x,bb.h+bb.y-y) for bb in BBS[ix]]

def get_datum(IMS, BBS):
    im, bbs = random_crop_and_bbs(IMS, BBS)
    im, bbs = aug(images = [im], bounding_boxes = [BoundingBox(*bb) for bb in bbs])
    im, bbs = im[0], from_imgaug_bbs(bbs)
    classification_map, regression_map, found_fld_bbs, found_fld_deltas, found_anchors = img2label(im, bbs)
    return im, classification_map, regression_map, found_fld_bbs, found_fld_deltas, found_anchors

def show_datum(im, classification_map, regression_map, found_fld_bbs, found_fld_deltas, found_anchors):
    fig, ax = plt.subplots(2, 1, figsize=(30,10))
    all_found_field_bbs = [y for x in found_fld_bbs.values() for y in x]
    all_found_field_txt = [x for x in found_fld_bbs.keys()]
    show(im, bbs=all_found_field_bbs, texts=all_found_field_txt, ax=ax[0])
    show(classification_map[0], ax=ax[1], title='Background')
    plt.show()
    ctr = 10
    for ix,fld in enumerate(FOUND_FIELDS):
        titles = ['class','dx','dy','dX','dY']
        iou_map = classification_map[ix+1]
        if iou_map.max() < 1: continue
        fig, ax = plt.subplots(1, 2, figsize=(30,10))
        logger.info(f'{FOUND_FIELDS[ix]} @ HeatMap #{ix}')
        show(iou_map, ax=ax[0], title=titles.pop(0))
        show(im, bbs=found_anchors[fld], ax=ax[1], title=found_fld_deltas[fld])
        plt.show()
        ctr -= 1
        if ctr == 0: break
            
def batch(IMS, BBS, batch_size=2):
    data = [get_datum(IMS, BBS) for _ in range(batch_size)]
    ims, classification_maps, regression_maps, _, _, _ = zip(*data)
    ims, classification_maps, regression_maps = [np.array(i) for i in [ims, classification_maps, regression_maps]]
    ims = 1 - ims[...,None]/255.

    classification_maps = classification_maps.transpose(0, 2, 3, 1) # B x R x C x (1+len(FOUND_FIELDS))
    classification_maps = classification_maps.reshape(batch_size, R*C, -1) # B x R*C x 4*(1+len(FOUND_FIELDS))

    regression_maps = regression_maps.transpose(0, 2, 3, 1)
    regression_maps = regression_maps.reshape(batch_size, R*C, -1)

    return ims, classification_maps, regression_maps

def hm2bb2(hm):
    argmax = np.argmax(hm)
    i,j = np.unravel_index(argmax, shape=(R,C))
    bb = (j*stride_w, i*stride_h, j*stride_w+sub_w, i*stride_h+sub_h)
    return BB(bb), (i,j)

def decode(im, classification_map, regression_map, debug=True, threshold=0.4):
    im = (1-im[...,0].copy()) * 255 if im.max() != 255 else im
    
    if len(classification_map.shape) == 2:
        classification_map = classification_map.detach().cpu().numpy() if isinstance(classification_map, torch.Tensor) else classification_map
        classification_map = classification_map.transpose(1, 0).reshape(-1, R, C)

        regression_map = regression_map.detach().cpu().numpy() if isinstance(regression_map, torch.Tensor) else regression_map
        regression_map = regression_map.transpose(1, 0).reshape(-1, R, C)

    assert classification_map.shape == (1+len(FOUND_FIELDS), R, C), classification_map.shape
    assert regression_map.shape     == (4*(1+len(FOUND_FIELDS)), R, C), regression_map.shape
    if classification_map.max() > 1.1: classification_map = F.softmax(torch.tensor(classification_map)).numpy()
    show(classification_map[0], sz=10, title='background-heat-map')
    for obj_ix, fld in enumerate(FOUND_FIELDS):
        iou_map = classification_map[obj_ix+1]
        if iou_map.max() < threshold: continue
        regr_map = regression_map[(obj_ix+1)*4:(obj_ix+2)*4]
        anc_bb, (i,j) = hm2bb2(iou_map)
        dx,dy,dX,dY = deltas = regr_map[0,i,j], regr_map[1,i,j], regr_map[2,i,j], regr_map[3,i,j]
        dx,dy,dX,dY = dx*W, dy*H, dX*W, dY*H
        corrected_bb = BB(anc_bb.x-dx, anc_bb.y-dy, anc_bb.X-dX, anc_bb.Y-dY)
        if debug:
            _bbs = [anc_bb,corrected_bb]
            show(im, bbs=_bbs, bb_colors=[(0,255,255),(255,0,255)], texts=[str(b) for b in _bbs], title=f'Correction of `{fld}` @ {deltas} @ Conf: {iou_map.max():.2f}') # cyan - anchor; pink - corrected
            # fig, ax = plt.subplots(1, 4, figsize=(20,10))
            # [show(regr_map[...,i], ax=ax[i])  for i in range(4)]; plt.show()
    # classification_map = classification_map.reshape(R, C, -1)

# ims, classification_maps, regression_maps = batch(1)
# inspect(ims, classification_maps, regression_maps)
# decode(ims[0], classification_maps[0], regression_maps[0])
