files = Glob('/rapids/notebooks/atcdata/OCR/Employer-Enrolement-Froms/Data/DATABASE-ERAPP-First-Pages/*.png')
len(files)

INPUT_FIELDS = [['Number of Locations',
         '# of Locations',
         '# Locations',
         'List Locations'],
         ['Industry Code',
         'Industry Code (SIC)',
         'Industry (SIC) Code'],
         ["Name of Workers' Compensation Carrier",
         "Workers' Comp Carrier Name"],
         ['Waiting Period waived for initial enrollees',
         'Waiving the initial waiting period',
         'Waiting Period for initial enrollees'],
         ["Group's Full Legal Name",
         "Group's/Company's Legal Name",
         "Group's Legal Name"],
         ['Contact Name',
         'Billing Contact/Title',
         'Billing Contact',
         'Contact Person'],
         ['Phone Number',
         'Billing Contact Phone',
         'Telephone'],
         ['Fax Number',
         'Fax'],
         ['E-Mail',
         'Email Address'],
         ['Tax ID Number',
         'Tax ID'],
         ['Number of Years in Existence',
         '# of Years in Business'],
         ['Nature of Business/Organization',
         'Nature of Business'],
         'Address(es) (or list on additional sheet of paper)',
         ['Effective Date',
         'Requested Effective Date'],
         ["Have Workers' Comp",
         "Have Worker's Comp"],
         'Group Name to appear on ID card (maximum 30 characters)',
         'Street Address',
         'City',
         'State',
         'Zip Code',
         'Names of Owners/Partners (if applicable)',
         'Internet access?',
         'Billing Address (If Different)',
         'Multi-Location Group*',
         'Organization Type',
         'Medical Benefit Plan Option',
         'Domestic Partner Coverage',
         'Did you have any employees other than yourself and your spouse during the preceding calendar year?',
         'X Calendar Year',
         'Waiting Period for new hires (Waiting period for medical coverage cannot exceed 90 days)',
         'Classes Excluded',
         "Names of Owners/Partners not covered by Workers' Comp",
         'Names of Persons currently on COBRA/Continuation, and/or Short/Long Term Disability',
         'Number of Persons currently on COBRA/Continuation, and/or Short/Long Term Disability (employees/dependents)',
         'Number of Employees Termed in last 12 Months',
         "Names of Owners/Partners not covered by Workers' Compensation",
         'Is the group subject to ERISA',
         'Will there be an Eligibility Waiting Period for New Hires',
         'List all subsidiaries to be included:',
         'If the majority of your employees are not located in your state of application',
         '# Eligible Employees',
         '# Ineligible Employees',
         'Total # Employees',
         'Hours per Week',
]

OTHER_FIELDS = [
    'Medical',
    'Dental',
    'Vision',
    'Basic EE Lifee/AD&D',
    'Basic Dep Lifee',
    'Supp EE Life/AD&D',
    'Basic Dep Life',
    'Supp EE Life/AD&D',
    'Supp Dep Life/AD&D',
    'STD',
    'STD Buy Up',
    'LTD',
    'LTD Buy Up',
    'Voluntary AD&D',
    'Other'
]

SAME_FIELDS = {v[0]:v for v in INPUT_FIELDS  if isinstance(v, list)}
SAME_FIELDS.update({v:[v] for v in INPUT_FIELDS  if isinstance(v, str)})

SAME_FIELDS_KV = {_v:k for k,v in SAME_FIELDS.items() for _v in v}
FIELDS = [_v for k,v in SAME_FIELDS.items() for _v in v]
len(FIELDS)

'''
for file in Tqdm(files[10:]):
    try:
        if file in DFS: continue
        image = read(file)
        DFS[file] = end2end(image)
    except KeyboardInterrupt:
        break
    except Exception as e:
        print(e)
%time dumpdill(DFS, 'enrolment.dfs')
'''
DFS = loaddill('enrolment.dfs')
logger.info(len(DFS))

'''_DFS = {}
for file in Tqdm(files):
    image = read(file)
    texts, confs, bbs, types = DFS[file]
    df = find_lines(texts,confs,bbs,eps=10)
    df = textify(df)
    count = 0
    possible_fields = FIELDS.copy()
    fld_locations = []
    inferences = []
    ixs = {}
    for ix,row in df.iterrows():
        text = row['text_lines']
        index, score = find_match(text, possible_fields, N=1, lower=True)
        inf = possible_fields[index]
        if score>70:
            # logger.debug(f'\nix:{ix}\ntext:{text}\ninf:{inf}\nscore:{score}')
            count += 1
            tag = SAME_FIELDS_KV[inf]
            inferences.append(tag)
            to_remove = SAME_FIELDS[tag]
            [possible_fields.remove(fld) for fld in to_remove]
            fld_locations.append(row)
            ixs[inf] = index

    fld_locations = pd.DataFrame(fld_locations)
    fld_locations.columns = ['Field','BBs','Conf']
    fld_locations['inference'] = inferences
    fld_locations['BB'] = fld_locations['BBs'].map(lambda x: combine_bbs(x))
    fld_locations.index = fld_locations.inference
    fld_locations.drop('inference', axis=1, inplace=True)
    _DFS[file] = fld_locations
dumpdill(_DFS, 'enrolment.fld_locations')'''

all_fld_locations = loaddill('enrolment.fld_locations')
FOUND_FIELDS = sorted(list(set([f for key in list(all_fld_locations.keys()) for f in all_fld_locations[key].index])))
# FOUND_FIELDS = ['City', 'State']
logger.info('\n\t'+'\n\t'.join([str(i) for i in enumerate(FOUND_FIELDS, 1)]))

from collections import defaultdict
from fieldDetection.object_detection import IOU
data = defaultdict(dict)

def get_ims_bbs(FOUND_FIELDS, )
ix = 0
for field in Tqdm(FOUND_FIELDS):
    for file in files:
        fld_locations = all_fld_locations[file]
        if field not in list(fld_locations.index): continue
        FieldBBs = combine_bbs(fld_locations.loc[field].BBs)
        data[file][field] = FieldBBs
    # if ix == 1: break
    # ix += 1

IMS, BBS = [], []

counter = 0
for k in Tqdm(data):
    IMS.append(read(k)[:1700,:1700])
    bbs = []
    for f in FOUND_FIELDS:
        if f in data[k]:
            bbs.append(data[k][f])
        else:
            bbs.append(BB(0,0,0,0))
    BBS.append(bbs)
    # show(im, bbs=bbs, sz=10)
# IMS, BBS = [BLANK] + IMS, [BLANK_TARGET] + BBS

# all_BBs = [bb for b in BBS for bb in b]
# wh = [(X-x,Y-y) for x,y,X,Y in all_BBs]
# print(len(wh))
# from sklearn.cluster import dbscan, KMeans
# kmeans = KMeans(n_clusters=5)
# kmeans = kmeans.fit(wh)
# print(kmeans.cluster_centers_)
# anchor_boxes = [(300,30),(1300,30),(170,30),(600,30)]
