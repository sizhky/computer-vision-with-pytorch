from ocr.loader import *
import torch, torchvision
import torch.nn as nn
from torch import optim
import torch.nn.functional as F

from .config import *
print('OBJDET',FOUND_FIELDS)

def conv_layer(ni,no,kernel_size,pool_size,stride=1,drop=0.2,bn=True,padding='same'):
    padding = 1 if kernel_size==3 else 0
    return nn.Sequential(
        nn.Conv2d(ni, no, kernel_size, stride, padding=padding),
        nn.ReLU(),
        nn.MaxPool2d(pool_size) if pool_size is not None else Identity(),
        nn.BatchNorm2d(no) if bn else Identity(),
        nn.Dropout(drop) if drop is not None else Identity()
    )
    
class Reshape(nn.Module):
    def __init__(self, *args):
        super(Reshape, self).__init__()
        self.shape = args

    def forward(self, x):
        return x.view(self.shape)
    
class Identity(nn.Module):
    def __init__(self)  :   super(Identity, self).__init__()
    def forward(self, x):   return x
    
class Permute(nn.Module):
    def __init__(self, *args):
        super(Permute, self).__init__()
        self.order = args

    def forward(self, x):
        return x.permute(*self.order)
    
def get_backbone(**kwargs):
    return nn.Sequential(
        conv_layer(1, 64, 3, pool_size=2),
        conv_layer(64, 128, 3, pool_size=2),
        conv_layer(128, 256, 3, pool_size=5),
        conv_layer(256, 512, 3, pool_size=(1,5), bn=False, drop=None),
        conv_layer(512, 512, 3, pool_size=None)
    )

def classification_head():
    return nn.Sequential(
        conv_layer(512, 512, 3, pool_size=None),
        nn.Conv2d(512, len(FOUND_FIELDS)+1, 3, padding=1),
        Reshape(-1, len(FOUND_FIELDS)+1, R*C),
        Permute(0, 2, 1)
    )
def regression_head():
    return nn.Sequential(
        conv_layer(512, 512, 3, pool_size=None),
        nn.Conv2d(512, 4*(len(FOUND_FIELDS)+1), 3, padding=1),
        Reshape(-1, 4*(len(FOUND_FIELDS)+1), R*C),
        Permute(0, 2, 1),
        # nn.Tanh()
    )

class objectDetection(nn.Module):
    def __init__(self):
        super().__init__()
        self.backbone, self.classification, self.regression = get_backbone().to(device), classification_head().to(device), regression_head().to(device)
        
    def forward(self, x):
        op = self.backbone(x)
        # print(op.shape)
        # a, b = nn.Conv2d(512, len(FOUND_FIELDS)+1, 3), nn.ReLU()
        # print(b(a(op)).shape)
        clss = self.classification(op)
        regr = self.regression(op)
        return clss, regr

# from torchsummary import summary
# summary(get_backbone(), (1,500,1700), device='cpu')

# m = objectDetection()
# ims, clss, regr = batch()
# inspect(ims, clss, regr, names=['ims','clss_true','regr_true'])
# ==================================================================
# IMS:
# Shape: (2, 500, 1700, 1)	Min: 0.0	Max: 1.0	Mean: 0.11148522029988464
# CLSS_TRUE:
# Shape: (2, 425, 3)	Min: 0.0	Max: 1.0	Mean: 0.3333333333333333
# REGR_TRUE:
# Shape: (2, 425, 12)	Min: -0.088	Max: 0.07705882352941176	Mean: -1.8835063437139566e-05
# ==================================================================
# outputs = m(torch.Tensor(ims).permute(0, 3, 1, 2))
# inspect(*outputs, names=['clss_pred','regr_pred'])
# ==================================================================
# CLSS_PRED:
# Shape: torch.Size([2, 425, 3])	Min: 0.0	Max: 2.611896514892578	Mean: 0.24075976014137268
# REGR_PRED:
# Shape: torch.Size([2, 425, 12])	Min: 0.0	Max: 0.9994010925292969	Mean: 0.19873003661632538
# ==================================================================
